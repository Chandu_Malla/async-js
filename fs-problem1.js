const fs = require('fs');
const { mkdir } = require('node:fs');

/**
 * Create a directory and process random files.
 * @param {string} absolutePathOfRandomDirectory - The absolute path of the target directory.
 * @param {number} randomNumberOfFiles - The random number of files to be processed.
 * @param {Function} callback - Callback function for directory creation.
 */
const fsProblem1 = (absolutePathOfRandomDirectory, randomNumberOfFiles) => {
    const callback = (err, directory, randomNumberOfFiles) => {
        if (err) {
            if (err.code === 'EEXIST') {
                console.log(`The ${directory} directory already exists`);
                createRandomFiles(directory, randomNumberOfFiles, () => {
                    console.log('All files created successfully.');
                });
            } else {
                console.error(err);
            }
        } else {
            console.log(`The ${directory} directory was created`);
            setTimeout(() => {
                createRandomFiles(directory, randomNumberOfFiles, () => {
                    console.log('All files created successfully.');
                });
            }, 500);
        }
    };

    mkdir(absolutePathOfRandomDirectory, (err) => {
        callback(err, absolutePathOfRandomDirectory, randomNumberOfFiles);
    });

    /**
     * Generate a random file name.
     * @returns {string} - Randomly generated file name.
     */
    const generateRandomString = () => {
        return `${Math.random().toString(36).slice(2, 10)}.json`;
    };

    /**
     * Create random .json files.
     * @param {string} absolutePathOfRandomDirectory - The absolute path of the target directory.
     * @param {number} randomNumberOfFiles - The random number of files to be processed.
     * @param {Function} callback - Callback function after all files are created.
     */
    const createRandomFiles = (absolutePathOfRandomDirectory, randomNumberOfFiles, callback) => {
        let filesCreated = 0;

        const createFile = () => {
            const fileName = generateRandomString();
            const filePath = `${absolutePathOfRandomDirectory}${fileName}`;

            fs.writeFile(filePath, '', (err) => {
                if (err) {
                    console.error(`Error creating ${filePath}`, err);
                } else {
                    console.log(`File ${fileName} created successfully`);
                    deleteFile(filePath, () => {
                        filesCreated++;
                        if (filesCreated < randomNumberOfFiles) {
                            createFile();
                        } else {
                            callback();
                        }
                    });
                }
            });
        };

        createFile();
    };

    /**
     * Delete a file.
     * @param {string} filePath - The path of the file to be deleted.
     * @param {Function} callback - Callback function after the file is deleted.
     */
    const deleteFile = (filePath, callback) => {
        fs.unlink(filePath, (err) => {
            if (err) {
                console.error(`Error deleting ${filePath}`, err);
            } else {
                console.log(`File at ${filePath} deleted successfully`);
            }
            callback();
        });
    };
};

// Example usage:
module.exports = fsProblem1