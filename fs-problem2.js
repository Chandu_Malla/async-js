const fs = require('fs');

const fsProblem2 = () => {
/**
 * Reads a file and processes its content.
 * @param {string} filePath - The path of the file to be read.
 * @param {Function} onDataCallback - Callback function for data events.
 * @param {Function} onEndCallback - Callback function for end events.
 * @param {Function} onErrorCallback - Callback function for error events.
 * @returns {ReadStream} - Read stream for the file.
 */
const createReadStream = (filePath, onDataCallback, onEndCallback, onErrorCallback) => {
    const readStream = fs.createReadStream(filePath, 'utf-8');

    readStream.on('data', onDataCallback);
    readStream.on('end', onEndCallback);
    readStream.on('error', onErrorCallback);

    return readStream;
};

/**
 * Function to store a filename in a file.
 * @param {string} fileName - The name of the file to be appended.
 */
const storeFileName = (fileName) => {
    fs.appendFile('../public/output/filenames.txt', `${fileName}\n`, (err) => {
        if (err) {
            console.error(err);
        } else {
            console.log(`${fileName} appended to filenames.txt`);
        }
    });
};

/**
 * Creates a new file with the provided name and data.
 * @param {string} fileName - The name of the new file.
 * @param {string} data - The data to be written to the file.
 */
const createNewFile = (fileName, data) => {
    fs.writeFile(`../public/output/${fileName}`, data, (err) => {
        if (err) {
            console.error(err);
        } else {
            console.log(`${fileName} was created successfully`);
            storeFileName(`${fileName}`);
        }
    });
};

/**
 * Main function for processing uppercase content of a file.
 */
const processUpperCase = () => {
    let dataInUpperCase = '';

    const readStream = createReadStream(
        '../public/lipsum_1.txt',
        (chunk) => {
            console.log(chunk);
            dataInUpperCase += chunk.toUpperCase();
        },
        () => {
            console.log("File successfully viewed");
            createNewFile('LIPSUM.txt', dataInUpperCase);

            // Sorts the data
            sortUpperCase = dataInUpperCase.split(' ').sort().join(' ')
            setTimeout(() => {
                processLowerCase();
            }, 500)

            setTimeout(() => {
                createNewFile('sort_LIPSUM.txt', sortUpperCase);
            }, 2000)

            setTimeout(() => {
                processFileNames()
            }, 3000)
        },
        (err) => {
            console.error('Error reading the file:', err);
        }
    );
};

/**
 * Main function for processing lowercase content of a file.
 */
const processLowerCase = () => {
    let dataInLowerCase = '';

    const readStreamData = createReadStream(
        '../public/output/LIPSUM.txt',
        (chunk) => {
            dataInLowerCase += chunk.toLowerCase();
            console.log(chunk);
        },
        () => {
            console.log("File successfully viewed for upper case");
            const data = dataInLowerCase.split(/[.]/)
                .filter(sentence => sentence.trim() !== '')
                .join(' ');

            createNewFile('lipsum.txt', data);

            // Sorts the data
            sortLowerCase = dataInLowerCase.split(' ').sort().join(' ')
            setTimeout(() => {
                createNewFile('sort_lipsum.txt', sortLowerCase);
            }, 2000)
        },
        (err) => {
            console.error('Error reading the file:', err);
        }
    );
};

/**
 * Deletes a file and calls the provided callback.
 * @param {string} filePath - The path of the file to be deleted.
 * @param {Function} callback - Callback function to be called after deletion.
 */
const deleteFile = (filePath, callback) => {
    if (fs.existsSync(filePath)) {
        fs.unlink(filePath, (err) => {
            if (err) {
                console.error(`Error deleting ${filePath}`, err);
            } else {
                console.log(`File at ${filePath} deleted successfully`);
            }

            // Call the callback whether there was an error or not
            if (callback) {
                callback(err);
            }
        });
    } else {
        console.log(`File at ${filePath} does not exist.`);

        // Call the callback with an error
        if (callback) {
            callback(new Error(`File at ${filePath} does not exist.`));
        }
    }
}

/**
 * Processes filenames from a file and deletes corresponding files.
 */
const processFileNames = () => {
    const readStream = createReadStream('../public/output/filenames.txt',
        (chunk) => {
            const lines = chunk.trim().split('\n');
            for (const line of lines) {
                console.log("Processing file:", line);

                const trimmedFileName = line.trim();
                const filePath = `../public/output/${trimmedFileName}`;
                deleteFile(filePath);
            }

            setTimeout(() => {
                console.log('All files are processed to delete')
            })
        },
        () => {
            // Clear the contents of filenames.txt after processing
            fs.writeFile('../public/output/filenames.txt', '', (err) => {
                if (err) {
                    console.error("Error clearing filenames.txt:", err);
                } else {
                    console.log(`All files from ./public/output/filenames.txt path are deleted`) 
                }
            });
            setTimeout(() => {
                console.log("filenames.txt data cleared");
            })
        },
        (err, chunk) => {
            console.error(`Error in deleting a file ${chunk}, ${err}`)
        }
    )
};

// Call the main functions
processUpperCase();

}

// Export the main function
module.exports = fsProblem2;