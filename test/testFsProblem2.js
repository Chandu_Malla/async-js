const fsProblem2 = require('./../fs-problem2'); // Update the path accordingly

// Define your test case function
const testFsProblem2 = () => {
    try {
        fsProblem2();
    } catch (error) {
        console.error(error);
    } finally {
        console.log('fsProblem2 test case was executed successfully');
    }
};

testFsProblem2()