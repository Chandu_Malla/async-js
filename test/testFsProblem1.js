const fsProblem1 = require('./../fs-problem1'); // Update the path accordingly

// Define your test case function
const testFsProblem1 = () => {
    try {
        fsProblem1('/home/chandu/Documents/Async JS/Random_dir/', 3);
    } catch (error) {
        console.error(error);
    } finally {
        console.log('fsProblem1 test case was executed successfully');
    }
};

testFsProblem1();