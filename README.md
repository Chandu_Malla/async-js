## Async JS

#### Description
First drill on Async JS:

Do not use Promises or async ..await.

#### Project structure
 Project structure as follows:
```
Folder structure:
        ├── fs-problem1.js
        ├── fs-problem2.js
        └── test
            ├── testFsProblem1.js
            └── testFsProblem2.js
```

#### Function Invoke
Once that is done, change the function signature of your `fs-problem1.js` exported function to be invoked as follows:

`fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles)`

#### Public folder
It consists the required data to solve these async problems

#### Test 
To test any individual file
```
node testFsProblem1
```
